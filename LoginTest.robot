*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${LOGIN URL}      http://kkeeper.com/
${BROWSER}        Chrome


*** Test Cases ***
Valid Login
    Open Browser To Login Page
    Input Username    demo
    Input Password    demo
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]    Close Browser

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Title Should Be    Kkeeper
	Sleep    1s
	Page Should Contain  Login

Input Username
    [Arguments]    ${username}
    Input Text    name=login    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    name=pass    ${password}

Submit Credentials
    Click Button    Sign in

Welcome Page Should Be Open
	Sleep    1s
    Page Should Contain  demo
